import time
from math import cos, pi
import math

class AnimationBase(object):
    DEFAULT_ANIM_TIME = 500
    def __init__(self, animTime):
        self._animTime = animTime if animTime else self.DEFAULT_ANIM_TIME
        self._startTime = None
        
    def animVal(self, fr, to, prog):
        if prog >= 1:
            return to
        
        return (to - fr) / 2. * (1. - cos(prog * pi)) + fr
    
    def oAnimVal(self, fr, to, prog):
        if prog >= 1:
            return to
        x = -8.*(prog - 0.3)
        return fr + (to - fr) * (1. - x / (x + math.exp(5.*(prog - 0.3))))
    
    def animate(self, curTime, graph):
        if not self._startTime:
            self._startTime = int(time.time() * 1000)
        prog = (curTime - self._startTime) / self._animTime
        self._nextStep(prog, graph)
        return prog < 1

class ZoomAnimation(AnimationBase):
    def __init__(self, axes, newXRange, newYRange, animTime=None):
        super(ZoomAnimation, self).__init__(animTime)
        self._ax = axes
        self._oldX = axes.get_xlim()
        self._newX = newXRange
        self._oldY = axes.get_ylim()
        self._newY = newYRange
        
    def _nextStep(self, prog, _graph):
        self._ax.set_xlim([self.animVal(fr, to, prog) for fr, to in zip(self._oldX, self._newX)])
        self._ax.set_ylim([self.animVal(fr, to, prog) for fr, to in zip(self._oldY, self._newY)])
        
class ModeNodesAnimation(AnimationBase):
    def __init__(self, oldPositions, newPositions, animTime=None):
        """
        pos: the position dictionary
        oldPositions: list of (x, y) tuples
        newPositions: list of (x, y) tuples
        """
        super(ModeNodesAnimation, self).__init__(animTime)
        self._from = oldPositions
        self._to = newPositions
        
    def _nextStep(self, prog, graph):
        oldNewTuples = [zip(fromTup, toTup) for fromTup, toTup in zip(self._from, self._to)]
        for i, node in enumerate(graph._pos):
            graph._pos[node] = [self.animVal(fr, to, prog) for fr, to in oldNewTuples[i]]
    
class MoveNodeAnimation(AnimationBase):
    def __init__(self, node, to, pos, animTime=None):
        super(MoveNodeAnimation, self).__init__(animTime)
        self._node = node
        self._from = pos[node]
        self._to = to
        
    def _nextStep(self, prog, graph):
        graph._pos[self._node] = [self.animVal(fr, to, prog) for fr, to in zip(self._from, self._to)]
        
class ScaleNodesAnimation(AnimationBase):
    def __init__(self, oldSizes, newSizes, animTime=None, overshoot=True):
        super(ScaleNodesAnimation, self).__init__(animTime)
        self._from = oldSizes
        self._to = newSizes
        if overshoot:
            self.animFunc = self.oAnimVal
        else:
            self.animFunc = self.animVal
        
    def _nextStep(self, prog, graph):
        i = 0
        while i < len(graph._nodeSizes):
            graph._nodeSizes[i] = self.animFunc(self._from[i], self._to[i], prog)
            i += 1

