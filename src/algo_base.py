from matplotlib import pyplot
from PyQt4.QtCore import QObject
from matplotlib import animation
import time
from utils import outDegree, effectiveDiameter, undirLengths, diameter
from matplotlib.backends import backend_qt4agg as backend
import networkx

class AlgorithmBase(QObject):
    SHOW_STATUS_TEXT = True
    YMARGINS = (0.4, 0.4)
    XMARGINS = (0.4, 0.4)
    
    def __init__(self, parent, window, draw=False):
        super(AlgorithmBase, self).__init__(parent)
        
        self._window = window
        
        if draw:
            self._fig, self._axes, self._xyLim = self._createFigure()
        else:
            self._fig = None
            self._axes = None
            self._xyLim = (0, 1)
        
        self._anim = None
        self._graphs = self._generateInitialGraphs()
        if draw:
            self.setColorScheme(0)
        self._initialize()
    
    def _createFigure(self, width=1, height=1):
        fig = pyplot.Figure(figsize=(width, height), dpi=100, frameon=True, facecolor='w')
        xyLim = (float(self._window.WIDTH) / 100., float(self._window.HEIGHT) / 100.)
        fig.add_axes((0, 0, 1, 1))
        
        axes = fig.gca()
        axes.set_axis_bgcolor('w')
        axes.set_xlim(0, xyLim[0])
        axes.set_ylim(0, xyLim[1])
        axes.set_axis_off()
        
        return fig, axes, xyLim
    
    def _initialize(self, start=False, draw=False):
        self._forceDraw = True
        if start:
            self.startAlgorithm(draw)

    def _getYRange(self, statusOffset=True):
        if self.SHOW_STATUS_TEXT and statusOffset:
            return (self.YMARGINS[0], self._xyLim[1] - self.YMARGINS[1] - 0.1)
        else:
            return (self.YMARGINS[0], self._xyLim[1] - self.YMARGINS[1])

    def _getXRange(self):
        return (self.XMARGINS[0], self._xyLim[0] - self.XMARGINS[1])

    def _generateInitialGraphs(self):
        """ returns a tuple of graphs """
        raise NotImplementedError("Implement in subclass")
        
    def getGraph(self):
        if self._graphs:
            return self._graphs[0]
        return None
        
    def startAlgorithm(self, draw=False):
        if draw:
            self.startAnimation()
    
    def _animate(self, _i=None):
        curTime = time.time() * 1000
        result = []
        for aGraph in self._graphs:
            result += [val for val in aGraph.animate(curTime, self._forceDraw) if val != None]
        
        self._forceDraw = False
        return result
    
    def startAnimation(self):
        if self._anim == None:
            self._anim = animation.FuncAnimation(self._fig, self._animate, init_func=self._animate, interval=1000 / 26, blit=True)
    
    def step(self, draw=False):
        if draw:
            self.draw()
        
    def draw(self):
        self._forceDraw = True
        
    def getFigure(self):
        return self._fig
    
    def clear(self, uncolor=True):
        if uncolor:
            for aGraph in self._graphs:
                aGraph.uncolor()
            
    def getColorSchemes(self):
        return None
        
    def setColorScheme(self, index):
        self.activeColorScheme = index
        colorScheme = self.getColorSchemes()[self.activeColorScheme][1]
        for aGraph in self._graphs:
            aGraph.setColorScheme(colorScheme)
        self.draw()
        
    def scaleGraphNodesByInDegree(self):
        self.getGraph().scaleNodesByInDegree()
        
    def scaleGraphNodesByOutDegree(self):
        self.getGraph().scaleNodesByOutDegree()
        
    def unscaleGraphNodes(self):
        self.getGraph().unscaleNodes()
        
    def setStatusText(self, text):
        if self.SHOW_STATUS_TEXT:
            self._window.setText(text)
        else:
            print text
        
    def showStats(self):
        nE, oD, diam, eDiam = self.getStats()
        self.setStatusText("#E %d  Out-Deg %.1f  Diam %.1f  E.Diam %.1f" % (nE, oD, diam, eDiam))
        
    def getStats(self):
        lengths = undirLengths(self.getGraph())
        return self.getGraph().number_of_edges(), outDegree(self.getGraph()), diameter(lengths), effectiveDiameter(lengths)
        
    def needsRelayout(self):
        return True
        
    def finish(self, draw=False, tryRerun=True):
        if tryRerun:
            try:
                self.step()
            except StopIteration:
                # re-run
                self.clear()
                self._initialize(start=True, draw=draw)
                return
        
        try:
            while True:
                self.step()
        except StopIteration:
            pass
        if draw:
            if self.needsRelayout():
                for aGraph in self._graphs:
                    aGraph.setPositions(None)
            else:
                for aGraph in self._graphs:
                    aGraph.uncolor()
            self.draw()
            self.showStats()

    def saveFigure(self, f):
        fig, axes, _ = self._createFigure(self._fig.get_figwidth(), self._fig.get_figheight())
        for aGraph in self._graphs:
            aGraph.drawToAxes(axes)
            
        canvas = backend.FigureCanvasQTAgg(fig)
        canvas.setMinimumSize(self._window.width(), self._window.height())
        canvas.setMaximumSize(self._window.width(), self._window.height())

        fig.savefig(f, format='pdf')
        
    def saveDot(self, f):
        for aGraph in self._graphs:
            networkx.write_dot(aGraph, f)
