import networkx as nx
from networkx import algorithms
from lca import tree_level_order_lowest_common_ancestor
import random
from cga_base import CGABase
import itertools

class DynamicCGA(CGABase):
    def __init__(self, parent, window, draw=False, fanout=3, difficulty=2.0, maxLevel=3):
        self._maxLevel = maxLevel
        super(DynamicCGA, self).__init__(parent, window, draw, fanout, difficulty)

    def _generateCommTree(self):
        # we start with a single node
        self._commTree.add_node(0)
        
    def getColorSchemes(self):
        return [("Community", self._communityColor), ("Cycle", self._cycleColor)]
        
    def _communityColor(self, n):
        if n == 0:
            return 0
        
        i = 1
        levelRange = None
        while True:
            levelRange = self._levelRange(i)
            if n in levelRange:
                break
            i += 1
            
        levelSize = self._fanout ** i
        myIndex = n - levelRange[0]
        # choose middle of my range
        return (0.2 + (2*myIndex + 1) * 0.9 / levelSize) / 2.
        
    def _cycleColor(self, n):
        maxNodes = sum([self.getFanout() ** i for i in range(self._maxLevel + 1)])
        n = n % maxNodes
        return 0.1 + 0.9 * float(n) / maxNodes
        
    def _graphRange(self):
        return range(self._commTree.number_of_nodes())
        
    def _distance(self, v, w, color=False):
        lca, pv, pw = tree_level_order_lowest_common_ancestor(self._commTree, v, w)
        
        if color:
            self._commTree.highlightNode(v)
            self._commTree.highlightNode(w)
            self._graph.highlightNode(v)
            self._graph.highlightNode(w)

            path1 = algorithms.shortest_path(self._commTree, lca, v)
            path2 = algorithms.shortest_path(self._commTree, lca, w)
            self._commTree.highlightPath(path=path1)
            self._commTree.highlightPath(path=path2)
        return pv + pw
    
    def _difficulty(self, dist):
        return self.difficulty ** (-float(dist)/2.)
        
    def _shouldConnect(self, v, w, color=False):
        dist = self._distance(v, w, color)
        d = self._difficulty(dist)
        rand = random.random() 
        con = rand < d
        if color:
            self.setStatusText("Distance: %s -> Probability: %s" % (dist, d))
        return con

    def startAlgorithm(self, draw=False):
        super(DynamicCGA, self).startAlgorithm(draw)
        self._pairIter = None
        self._height = 0
        if draw:
            self.clear()
            self.draw()
            self.setStatusText("Start algorithm, difficulty: %.1f" % self.difficulty)
    
    def step(self, draw=False):
        super(DynamicCGA, self).step(draw)
        
        res = None
        if self._pairIter == None:
            # add new nodes
            if self._height >= self._maxLevel:
                # maximum number of levels reached
                raise StopIteration("Algorithm finished.")
            
            self._height += 1
            nx.balanced_tree(self.getFanout(), self._height, self._commTree)
            if draw:
                self._graph.insertNodes(self._levelRange(self._height), animate=True, relayout=True)
                self._commTree.animateInsertion(relayout=True)
            
                # prapare pair iter for next steps
                # try all connections between new nodes and any node in the graph (form out-links)
                pairs = [(v, w) for v in self._levelRange(self._height) for w in self._graphRange()]
                random.shuffle(pairs)
                self._pairIter = iter(pairs)
                self.clear()
                self.setStatusText("Level %d -> added %d vertices" % (self._height, self._fanout ** self._height))
            else:
                self._graph.add_nodes_from(self._levelRange(self._height))
                # no need to materialize and shuffle in this case
                self._pairIter = itertools.product(self._levelRange(self._height), self._graphRange())
        
        else:
            # check if there are nodes to check
            try:
                if self._pairIter == None:
                    raise StopIteration()
                v, w = self._pairIter.next()
                while v == w:
                    v, w = self._pairIter.next()
                # try to connect node
                if draw:
                    self.clear()
                    if self._shouldConnect(v, w, color=True):
                        self._graph.connectNodes(v, w, highlight=draw)
                else:
                    if self._shouldConnect(v, w, color=False):
                        self._graph.add_edge(v, w)
            except StopIteration:
                # no edges left to check -> finish current level
                self._pairIter = None
                if draw:
                    self.clear()
                    self.showStats()
                res = True

        if draw:
            self.draw()
        return res
