import matplotlib
import os
matplotlib.use('qt4agg')
from matplotlib.backends import backend_qt4agg as backend
from PyQt4.QtGui import QMainWindow, QVBoxLayout, QMenu, QKeySequence, QLabel, QFont, QSizePolicy
from PyQt4.QtCore import Qt, QSize
from functools import partial

class AlgorithmWindow (QMainWindow):
    WIDTH = 1024
    HEIGHT = 768
    FONT_SIZE = 24
    FN_BASE = "fig"
    def __init__(self, algoGen):
        super(AlgorithmWindow, self).__init__()
        
        self.setMinimumSize(self.WIDTH, self.HEIGHT)
        self.setMaximumSize(self.WIDTH, self.HEIGHT)
        self.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        
        self.algo = algoGen(self, self)
        
        canvas = backend.FigureCanvasQTAgg(self.algo.getFigure())
        
        canvasLayout = QVBoxLayout(canvas)
        canvasLayout.setContentsMargins(5, 2, 0, 0)
        self._label = QLabel(canvas)
        oldFont = self._label.font()
        self._label.setFont(QFont(oldFont.family(), self.FONT_SIZE))
        canvasLayout.addWidget(self._label, 1, Qt.AlignTop)
        
        self.algo.startAlgorithm(draw=True)
    
        menuBar = self.menuBar()
        algoMenu = QMenu("Algorithm", self)
        algoMenu.addAction("Step", partial(self.algo.step, True), QKeySequence.fromString("S"))
        algoMenu.addAction("Finish", partial(self.algo.finish, True), QKeySequence.fromString("CTRL+R"))
        algoMenu.addAction("Save Step", self.saveStep, QKeySequence.fromString("CTRL+S"))
        menuBar.addMenu(algoMenu)
        
        layoutMenu = QMenu("Layout", self)
        layoutMenu.addAction("Relayout", self.algo._graph.relayout, QKeySequence.fromString("CTRL+L"))
        colorSchemeMenu = QMenu("Color Scheme", self)
        for i, scheme in enumerate(self.algo.getColorSchemes()):
            colorSchemeMenu.addAction(scheme[0], partial(self.changeColorScheme, i), "CTRL+%d" % (i + 1))
        layoutMenu.addMenu(colorSchemeMenu)
        layoutMenu.addAction("Scale vertices by in-degree", self.scaleInDegree, "CTRL+i")
        layoutMenu.addAction("Scale vertices by out-degree", self.scaleOutDegree, "CTRL+o")
        layoutMenu.addAction("Reset vertex sizes", self.unscale, "CTRL+0")
        layoutMenu.addAction("Increase Font Size", self.increaseFontSize, "CTRL++")
        layoutMenu.addAction("Decrease Font Size", self.decreaseFontSize, "CTRL+-")
        layoutMenu.addAction("Toggle Full Screen", self.toggleFullScreen, "CTRL+F")
        menuBar.addMenu(layoutMenu)
        
        self.setCentralWidget(canvas)

    def sizeHint(self):
        return QSize(self.WIDTH, self.HEIGHT)

    def toggleFullScreen(self):
        if (int(self.windowState()) & Qt.WindowFullScreen) != 0:
            self.showNormal()
        else:
            self.showFullScreen()

    def changeColorScheme(self, index):
        self.algo.setColorScheme(index)
        self.algo.clear(uncolor=False)
        
    def increaseFontSize(self):
        oldFont = self._label.font()
        self._label.setFont(QFont(oldFont.family(), oldFont.pointSize() + 2))
        
    def decreaseFontSize(self):
        oldFont = self._label.font()
        self._label.setFont(QFont(oldFont.family(), oldFont.pointSize() - 2))
        
    def scaleInDegree(self):
        self.algo.scaleGraphNodesByInDegree()
        
    def scaleOutDegree(self):
        self.algo.scaleGraphNodesByOutDegree()
        
    def unscale(self):
        self.algo.unscaleGraphNodes()
        
    def setText(self, text):
        self._label.setText(text)
        
    def saveStep(self):
        fn_base = self.FN_BASE
        i = 0
        while os.path.exists("./%s%02d.pdf" % (fn_base, i)):
            i = i + 1
        
        fname = "./%s%02d.pdf" % (fn_base, i)
        print "--> %s" % fname
        self.algo.saveFigure(fname)
        #self.algo.saveDot(fname + ".dot")
