import networkx as nx
from animation import ModeNodesAnimation, ZoomAnimation, ScaleNodesAnimation
from PyQt4.QtCore import QObject
import matplotlib
from operator import add, sub, div, mul
from networkx import algorithms
import itertools
from matplotlib import colors
import numpy as np

class MyGraph(QObject, nx.DiGraph):
    ELW = 1.5  # default line width
    NLW = 1.  # default node line width
    EC = 'k'  # default edge color
    EHC = 'r'  # highlighted edge color
    NC = 0.  # default node color
    NS = 200.  # default node size
    LABELS = False  # draw labels
    AL = 0.25  # arrow length
    SF = 100 # node degree scale factor
    
    def __init__(self, parent, nodePositions=None, edges=None, fig=None, xRange=(0, 1), yRange=(0, 1), arrows=True):
        QObject.__init__(self, parent)
        nx.DiGraph.__init__(self)
        
        self.clearAll()
        minMax = zip(xRange, yRange)
        self._xyMin = minMax[0]  # [xMin, yMin]
        self._xyRange = map(sub, minMax[1], self._xyMin)  # [xRange, yRange]
        self._colorForNode = lambda _i : self.NC
        self.defaultLayout = lambda g : nx.graphviz_layout(g, prog='neato')
        self._arrows = arrows

        # Graph initialization
        if nodePositions:
            self.add_nodes_from(range(len(nodePositions)))
            if edges:
                self.add_edges_from(edges)
            self._pos = self._generatePositionDict(nodePositions, uncolor=True)
    
        if not fig:
            fig = matplotlib.figure.Figure(figsize=(5, 4), dpi=100)
        self._axes = fig.gca()

    def setColorScheme(self, colorFunc):
        self._colorForNode = colorFunc
        self._resetNodeColors()

    def _generatePositionDict(self, nodePositions, uncolor=False):
        if nodePositions != None:
            if type(nodePositions) == dict:
                posDict = {i : nodePositions[i] for i in nodePositions}
            elif type(nodePositions) == list:
                posDict = {i : nodePositions[i] for i in range(len(nodePositions))}
            else:
                raise Exception("Unknown data type: %s", type(nodePositions))
            if len(posDict) == 0:
                return posDict
            if len(posDict) == 1:
                posDict = {n : (0.5, 0.5) for n in posDict}  # position node in center
            else:
                xyMin = map(min, zip(*(posDict.values())))  # [xMax, yMax]
                xyRange = map(sub, map(max, zip(*(posDict.values()))), xyMin)  # [xRange, yRange]
                
                # normalize: subtract minima, divide through ranges
                posDict = {i : map(div, map(sub, posDict[i], xyMin), xyRange) for i in posDict}
            
            # transform in graph range: multiply by range, add minima
            posDict = {i : map(add, map(mul, posDict[i], self._xyRange), self._xyMin) for i in posDict}
            if uncolor:
                self.uncolor()
            
            return posDict
        else:
            return self._generatePositionDict(self.defaultLayout(self), uncolor)
    
    def relayout(self):
        self.moveNodes(self.defaultLayout(self))
            
    def setPositions(self, pos):
        self._pos = self._generatePositionDict(pos, uncolor=(pos == None))
    
    def drawToAxes(self, ax):
        mpl_nodes = nx.draw_networkx_nodes(self,
                                                 self._pos,
                                                 ax=ax,
                                                 node_color=self._nodeColors,
                                                 cmap=colors.LinearSegmentedColormap.from_list('map', [(0, (1, 1, 1)),
                                                                                                       (0.1, (0.18, 0.98, 0.00)),
                                                                                                       (0.28, (0.98, 0.98, 0.06)),
                                                                                                       (0.46, (1.00, 0.07, 0.13)),
                                                                                                       (0.64, (1.00, 0.06, 0.98)),
                                                                                                       (0.82, (0.08, 0.07, 1.00)),
                                                                                                       (1.00, (0.06, 1.00, 0.99))]),
                                                 linewidths=self._nodeLineWidths,
                                                 vmin=0., vmax=1.,
                                                 node_size=self._nodeSizes)
        
        
        mpl_edges = nx.draw_networkx_edges(self,
                                            self._pos,
                                            ax=ax,
                                            edge_color=self._edgeColors,
                                            width=self._edgeWidths,
                                            arrows=self._arrows,
                                            al=self.AL)
        
        if self.LABELS:
            labels = nx.draw_networkx_labels(self, self._pos, ax=ax).values()
        else:
            labels = None
        
        return mpl_nodes, mpl_edges, labels
    
    def _draw(self, pos=None, ax=None):
        if self.number_of_nodes() == 0:
            return
        if self._pos == None:
            self._pos = self._generatePositionDict(pos, uncolor=True)

        if ax == None:
            ax = self._axes
            
        self._mpl_nodes, self._mpl_edges, labels = self.drawToAxes(ax)
        
        if self.LABELS:
            for l in labels:
                l.set_animated(True)
        
        if self._arrows and self._mpl_edges != None:
            self._mpl_edges, self._mpl_arrows = self._mpl_edges
        
        if self._mpl_nodes != None:
            self._mpl_nodes.set_animated(True)
        if self._mpl_edges != None:
            self._mpl_edges.set_animated(True)
            if self._arrows:
                self._mpl_arrows.set_animated(True)
        
        self._artists = [self._mpl_arrows, self._mpl_edges, self._mpl_nodes]
        if self.LABELS:
            self._artists += labels
        return self._artists

    def setVisibleSpace(self, xRange, yRange, animate=False):
        if animate:
            self._animations.append(ZoomAnimation(self._axes, xRange, yRange))
        else:
            self._axes.set_xlim(xRange)
            self._axes.set_ylim(yRange)
            
    def _resetEdgeFormat(self):
        numEdges = self.number_of_edges()
        if self._edgeColors == None or len(self._edgeColors) != self.number_of_edges():
            self._edgeColors = np.empty(numEdges, dtype=type(self.EC))
        self._edgeColors.fill(self.EC)
        if self._edgeWidths == None or len(self._edgeWidths) != self.number_of_edges():
            self._edgeWidths = np.empty(numEdges, dtype=type(self.ELW))
        self._edgeWidths.fill(self.ELW)
            
    def _resetNodeColors(self):
        if self._nodeColors == None or len(self._nodeColors) != self.number_of_nodes():
            self._nodeColors = np.empty(self.number_of_nodes(), dtype=type(self.NC))
        for i, n in enumerate(self.nodes()):
            self._nodeColors[i] = self._colorForNode(n)
            
    def _resetNodeFormat(self):
        numNodes = self.number_of_nodes()
        if self._nodeLineWidths == None or len(self._nodeLineWidths) != self.number_of_nodes():
            self._nodeLineWidths = np.empty(numNodes, dtype=type(self.NLW))
        self._nodeLineWidths.fill(self.NLW)
        if self._nodeSizes == None or len(self._nodeSizes) != self.number_of_nodes():
            self._nodeSizes = np.empty(numNodes, dtype=type(self.NS))
        self._nodeSizes.fill(self.NS)
        self._resetNodeColors()
            
    def uncolor(self):
        self._edgeOptions = {}

        self._resetEdgeFormat()
        self._resetNodeFormat()        
        # self._edgeColors = [self.EC] * self.number_of_edges()
        # self._edgeWidths = [self.ELW] * self.number_of_edges()
        # self._nodeColors = [self._colorForNode(i) for i in self.nodes()]
        # self._nodeLineWidths = [self.NLW] * self.number_of_nodes()
        # self._nodeSizes = [self.NS] * self.number_of_nodes()
        
    def clearAll(self):
        self.clear()
        self._mpl_edges = None
        self._mpl_arrows = None
        self._mpl_nodes = None
        self._artists = []
        self._animations = []
        self._pos = None
        self._edgeOptions = None
        self._edgeColors = None
        self._edgeWidths = None
        self._nodeColors = None
        self._nodeLineWidths = None
        self._nodeSizes = None
        self.clearPlot()
    
    def highlightNode(self, node):
        """ Makes a node bigger """
        self._nodeLineWidths[node - self.nodes()[0]] = self.NLW * 2
        self._nodeSizes[node - self.nodes()[0]] = self.NS * 2
        
    def dehighlightNode(self, node):
        """ Reverts a node to its default size """
        self._nodeLineWidths[node - self.nodes()[0]] = self.NLW
        self._nodeSizes[node - self.nodes()[0]] = self.NS
        
    def colorNode(self, node, color=None):
        """ Colors a node """
        color = color if color != None else self.NC
        self._nodeColors[node - self.nodes()[0]] = color
        
    def colorEdge(self, fr, to, color=None, width=None):
        if (fr, to) in self._edgeOptions:
            options = self._edgeOptions[(fr, to)]
        else:
            options = {}
        
        color = color if color != None else self.EC
        width = width if width != None else self.ELW
        
        options['c'] = color
        options['w'] = width
        self._edgeOptions[(fr, to)] = options
        
        for i, e in enumerate(self.edges()):
            f, t = e
            if f == fr and t == to:
                self._edgeColors[i] = color
                self._edgeWidths[i] = width
                break
        
    def _reformatEdges(self):
        for i, anEdge in enumerate(self.edges()):
            if anEdge in self._edgeOptions:
                options = self._edgeOptions[anEdge]
                if 'c' in options:
                    self._edgeColors[i] = options['c']
                if 'w' in options:
                    self._edgeWidths[i] = options['w']
            
    def colorPath(self, fr=None, to=None, path=None, color=None, width=None):
        if path == None:
            path = algorithms.shortest_path(self, fr, to)
        
        it1, it2 = itertools.tee(path)
        it2.next()
        for v, w in itertools.izip(it1, it2):
            self.colorEdge(v, w, color, width)

    def highlightPath(self, fr=None, to=None, path=None):
        self.colorPath(fr, to, path, self.EHC, width=self.ELW * 2)
        
    def highlightEdge(self, fr, to):
        self.colorEdge(fr, to, self.EHC, width=self.ELW * 2)
        
    def dehighlightEdge(self, fr, to):
        self.colorEdge(fr, to, self.EC, width=self.ELW)

    def scaleNodesByInDegree(self, factor=None):
        if factor == None:
            factor = self.SF
        newSizes = np.empty(self.number_of_nodes(), dtype=int)
        degIter = self.in_degree_iter()
        for i, pair in enumerate(degIter):
            newSizes[i] = self.NS + factor * pair[1] 
        self._animations.append(ScaleNodesAnimation(np.copy(self._nodeSizes), newSizes, overshoot=False))
        
    def scaleNodesByOutDegree(self, factor=None):
        if factor == None:
            factor = self.SF
        newSizes = np.empty(self.number_of_nodes(), dtype=int)
        degIter = self.out_degree_iter()
        for i, pair in enumerate(degIter):
            newSizes[i] = self.NS + factor * pair[1]
        self._animations.append(ScaleNodesAnimation(np.copy(self._nodeSizes), newSizes, overshoot=False))

    def unscaleNodes(self):
        newSizes = np.empty(self.number_of_nodes(), dtype=int)
        newSizes.fill(self.NS)
        self._animations.append(ScaleNodesAnimation(np.copy(self._nodeSizes), newSizes, overshoot=False))

    def nodesInserted(self, nodes, highlight=False, color=None, animate=False, relayout=False):
        ns = self.NS if highlight == False else self.NS * 2
        for v in nodes:
            self._nodeColors = np.append(self._nodeColors, [color if color != None else self._colorForNode(v)])
            self._nodeLineWidths = np.append(self._nodeLineWidths, [self.NLW if highlight == False else self.NLW * 2])
            self._nodeSizes = np.append(self._nodeSizes, [ns if animate == False else 0.])
        if animate:
            if relayout:
                newPositions = self._generatePositionDict(self.defaultLayout(self))
                for v in nodes:
                    self._pos[v] = newPositions[v]
                self.moveNodes(newPositions)
            
            newSizes = np.copy(self._nodeSizes)
            offset = self.nodes()[0]
            for v in nodes:
                newSizes[v - offset] = ns
            self._animations.append(ScaleNodesAnimation(np.copy(self._nodeSizes), newSizes))
        elif relayout:
            self.setPositions(None)

    def insertNode(self, v, highlight=False, color=None, animate=False, relayout=False):
        self.insertNodes([v], highlight, color, animate, relayout)

    def insertNodes(self, nodes, highlight=False, color=None, animate=False, relayout=False):
        self.add_nodes_from(nodes)
        self.nodesInserted(nodes, highlight, color, animate, relayout)
            
    def animateInsertion(self, highlight=False, color=False, relayout=False):
        newNodes = set(self.nodes()) - set(self._pos.keys())
        self.nodesInserted(newNodes, highlight, color, True, relayout)

    def connectNodes(self, v, w, highlight=False, color=None):
        if w not in self[v]:
            self.add_edge(v, w)
            if self._edgeColors != None:
                self._resetEdgeFormat()
                self._reformatEdges()
                if highlight:
                    c = color
                    if c == None:
                        c = self.EHC
                    self.colorEdge(v, w, c, self.ELW * 2)
            
    def clearPlot(self):
        if self._artists:
            for a in self._artists:
                if a != None:
                    a.remove()
        self._artists = []

    def moveNodes(self, newPositions):
        if type(newPositions) == list:
            myNodes = self.nodes()
            newPositions = {myNodes[i] : newPositions[i] for i in len(newPositions)}
        
        newPositions = self._generatePositionDict(newPositions).values()
        self._animations.append(ModeNodesAnimation(self._pos.values(), newPositions))

    # Animation function
    def animate(self, curTime, force=False):
        if not force and not self._animations:
            return self._artists
            
        self._animations = [anim for anim in self._animations if anim.animate(curTime, self)]
        return self._draw()
    
