import matplotlib
from graph import MyGraph
matplotlib.use('qt4agg')
import sys
from PyQt4.QtGui import QApplication
from algo_base import AlgorithmBase
from cga_basic import BasicCGA
from cga_dynamic import DynamicCGA
from forest_fire import ForestFire
from algo_window import AlgorithmWindow
from optparse import OptionParser

def visualizeAlgorithm(algoGen):
    app = QApplication(sys.argv)
    
    win = AlgorithmWindow(algoGen)
    
    win.show()
    win.raise_()
    
    sys.exit(app.exec_())

def appendResults(algo, d, results):
    graph = algo.getGraph()
    
    n = graph.number_of_nodes()
    
    # append row header
    if n in results:
        resDict = results[n]
    else:
        resDict = {}
        results[n] = resDict
    
    if d in resDict:
        #already computed this one
        return

    res = algo.getStats()
    resDict[d] = res

def testAlgo(algoGen, sizes, params, fn):
    with open(fn, "wb") as outFile:
        header = "N\t" + '\t'.join(str(d) + 
                                   " (Edges)\t" + str(d) + 
                                   " (Degree)\t" + str(d) + 
                                   " (Diameter)\t" + str(d) + 
                                   " (Eff.Diameter)" for d in params)
        print header
        outFile.write(header)
        outFile.write("\n")

        results = {}
        for d in params:
            nextSize = 1
            for s in sizes:
                algo = algoGen(s, d)
                algo.startAlgorithm()
                try:
                    while True:
                        if algo.step():
                            if algo.getGraph().number_of_nodes() >= nextSize:
                                appendResults(algo, d, results)
                                sys.stderr.write("\r(%d/%d) %d       " % (params.index(d) + 1, len(params), algo.getGraph().number_of_nodes()));
                                while nextSize <= algo.getGraph().number_of_nodes():
                                    nextSize *= 2
                                
                except StopIteration:
                    pass
                
                appendResults(algo, d, results)
    
        
        for n in results:
            paramResults = (results[n][d] for d in params)
            line = "%d\t" % n + '\t'.join('\t'.join((str(v) for v in res)) for res in paramResults)
            print line
            outFile.write(line)
            outFile.write('\n')

def test():
    print "Basic Community Guided Attachment"
    d_vals = [1.5, 2, 3]
    testAlgo(lambda s, d : BasicCGA(None, None, height=s, difficulty=d), range(8), d_vals, 'bcga.tsv')
            
    print
    print "Dynamic Community Guided Attachment"
    testAlgo(lambda s, d : DynamicCGA(None, None, maxLevel=s, difficulty=d), [7], d_vals, 'dcga.tsv')
            
    print
    print "Forest Fire"
    pars = [(0.35,0.2), (0.37,0.32), (0.38,0.35)]
    testAlgo(lambda s, par : ForestFire(None, None, maxNodes=s, p=par[0], r=par[1]), [2048], pars, 'ff.tsv')

if __name__ == '__main__':
    def parse_args():
        usage = "usage: %prog [options]"
        optionParser = OptionParser(usage=usage)
        optionParser.add_option("--test", "-t",
                          default=False, dest="test", action="store_true",
                          help="Analyze algorithm results.")
        optionParser.add_option("--bcga", "-b",
                          default=False, dest="bcga", action="store_true",
                          help="Visualize Basic CGA.")
        optionParser.add_option("--dcga", "-d",
                          default=False, dest="dcga", action="store_true",
                          help="Visualize Dynamic CGA.")
        optionParser.add_option("--ff", "-f",
                          default=False, dest="ff", action="store_true",
                          help="Visualize Forest Fire.")
        optionParser.add_option("--ns",
                          action="store", type="int", dest="ns", default=MyGraph.NS,
                          help="Set the node size.")
        optionParser.add_option("--labels",
                          default=False, dest="labels", action="store_true",
                          help="Draw labels into nodes.")
        optionParser.add_option("--no-status",
                          default=False, dest="noStatus", action="store_true",
                          help="Do not draw status text.")
        optionParser.add_option("--mb",
                          action="store", type="float", dest="mb", default=AlgorithmBase.YMARGINS[0],
                          help="Set the bottom margin")
        optionParser.add_option("--mt",
                          action="store", type="float", dest="mt", default=AlgorithmBase.YMARGINS[1],
                          help="Set the top margin")
        optionParser.add_option("--width",
                          action="store", type="int", dest="width", default=AlgorithmWindow.WIDTH,
                          help="Set the window width.")
        optionParser.add_option("--height",
                          action="store", type="int", dest="height", default=AlgorithmWindow.HEIGHT,
                          help="Set the window height.")
        optionParser.add_option("--fn-base",
                          action="store", dest="fnBase", default=AlgorithmWindow.FN_BASE,
                          help="Set the figure name base.")
        optionParser.add_option("--al",
                          action="store", type='float', dest="al", default=MyGraph.AL,
                          help="Set the arrow length.")
        optionParser.add_option("--nsf",
                          action="store", type='int', dest="nsf", default=MyGraph.SF,
                          help="Set the node degree scale factor.")
        return optionParser.parse_args()
    
    (options, args) = parse_args()
    
    MyGraph.LABELS = options.labels
    MyGraph.NS = options.ns
    MyGraph.AL = options.al
    MyGraph.SF = options.nsf
    AlgorithmBase.SHOW_STATUS_TEXT = not options.noStatus
    AlgorithmBase.YMARGINS = (options.mb, options.mt)
    AlgorithmWindow.WIDTH = options.width
    AlgorithmWindow.HEIGHT = options.height
    AlgorithmWindow.FN_BASE = options.fnBase
        
    if options.test:
        test()
    if options.bcga:
        visualizeAlgorithm(lambda parent, window : BasicCGA(parent, window, draw=True))
    if options.dcga:
        visualizeAlgorithm(lambda parent, window : DynamicCGA(parent, window, draw=True))
    if options.ff:
        visualizeAlgorithm(lambda parent, window : ForestFire(parent, window, draw=True))

    
