import networkx as nx
from networkx import algorithms
from lca import tree_level_order_lowest_common_ancestor
import random
from cga_base import CGABase
import itertools

class BasicCGA(CGABase):
    def __init__(self, parent, window, draw=False, fanout = 3, height = 3, difficulty=2):
        self._height = height
        super(BasicCGA, self).__init__(parent, window, draw, fanout, difficulty)

    def _generateCommTree(self):
        nx.balanced_tree(self.getFanout(), self._height, self._commTree)
        
    def _colorForNode(self, n):
        offset = sum([self._fanout ** i for i in range(self._height)])
        if n < offset:
            return 0
        return 0.1 + 0.9 * float(n - offset) / (self._fanout ** (self._height))
        
    def _graphRange(self):
        return self._levelRange(self._height)
        
    def _h(self, v, w, color=False):
        #lca = lowest_common_ancestor(self._commTree, v, w)
        lca, pv, _pw = tree_level_order_lowest_common_ancestor(self._commTree, v, w)
        
        if color:
            self._commTree.highlightNode(v)
            self._commTree.highlightNode(w)
            self._graph.highlightNode(v)
            self._graph.highlightNode(w)
            self._commTree.highlightNode(lca)
        
            path = algorithms.shortest_path(self._commTree, lca, v)
            self._commTree.highlightPath(path=path)
            self._commTree.highlightPath(lca, w)
        return pv
    
    def _difficulty(self, h):
        return self.difficulty ** (-h)
        
    def _shouldConnect(self, v, w, color=False):
        h = self._h(v, w, color)
        d = self._difficulty(h)
        rand = random.random()
        con = rand < d
        if color:
            self.setStatusText("Distance: %s -> Probability: %s" % (2*h, d))
        return con

    def startAlgorithm(self, draw=False):
        super(BasicCGA, self).startAlgorithm(draw)
        if draw:
            self.setStatusText("Start algorithm, difficulty: %.1f" % self.difficulty)
            pairs = [(v, w) for v in self._graphRange() for w in self._graphRange()]
            random.shuffle(pairs)
            self.pairIter = iter(pairs)
            self.clear()
            self.draw()
        else:
            # no need to materialize and shuffle cross product in this case
            self.pairIter = itertools.product(self._graphRange(), repeat=2)
    
    def needsRelayout(self):
        return False
    
    def step(self, draw=False):
        super(BasicCGA, self).step(draw)
        
        v, w = self.pairIter.next()
        while v == w:
            v, w = self.pairIter.next()
        
        if draw:
            self.clear()
            if self._shouldConnect(v, w, color=draw):
                self._graph.connectNodes(v, w, highlight=draw)
            self.draw()
        else:
            if self._shouldConnect(v, w, color=draw):
                self._graph.add_edge(v, w)
