from graph import MyGraph
import networkx as nx
from algo_base import AlgorithmBase

class CGABase(AlgorithmBase):
    def __init__(self, parent, window, draw, fanout, difficulty):
        self._fanout = fanout
        self.difficulty = difficulty
        super(CGABase, self).__init__(parent, window, draw)
        
    def _generateInitialGraphs(self):
        xRange = self._getXRange()
        halfRange = (xRange[1]- xRange[0]) * 2. / 3.
        margin = (xRange[1]- xRange[0]) / 20.
        
        
        self._commTree = MyGraph(self, fig=self._fig, xRange=(xRange[0], halfRange - margin), yRange=self._getYRange(), arrows=False)
        self.activeColorScheme = 0
        self._commTree.defaultLayout = lambda g : nx.graphviz_layout(g, prog='dot')
        
        self._graph = MyGraph(self, fig=self._fig, xRange=(halfRange + margin, xRange[1]), yRange=self._getYRange(False))
        return self._graph, self._commTree
        
    def startAlgorithm(self, draw=False):
        self._commTree.clearAll()
        self._generateCommTree()
        if draw:
            pass
            #self._relayoutCommTree()
        
        self._graph.clearAll()
        self._graph.add_nodes_from(self._graphRange())
        super(CGABase, self).startAlgorithm(draw)
        
    def getColorSchemes(self):
        return [("Default", self._colorForNode)]
        
    def _generateCommTree(self):
        raise NotImplementedError("Implement in subclass")
        
    def _colorForNode(self, n):
        raise NotImplementedError("Implement in subclass")
        
    def _graphRange(self):
        raise NotImplementedError("Implement in subclass")
        
    def _shouldConnect(self):
        raise NotImplementedError("Implement in subclass")
    
    def _relayoutCommTree(self, animate=False):
        if animate:
            self._commTree.relayout()
        else:
            self._commTree.setPositions(None)
        
    def _relayoutGraph(self, animate=False):
        if animate:
            self._graph.relayout()
        else:
            self._graph.setPositions(None)
    
    def getFanout(self):
        return self._fanout
    
    def _levelRange(self, level):
        offset = sum([self._fanout ** i for i in range(level)])
        return range(offset,
                     offset + self._fanout ** (level))
