from graph import MyGraph
from algo_base import AlgorithmBase
import random
import numpy.random
import sys

class ForestFire(AlgorithmBase):
    def __init__(self, parent, window, draw=False, p = 0.25, pb = 0.2, maxNodes = 50):
        """
        p: Forward burning probability
        r: Backward burning ratio
        maxNodes: Maximum number of nodes added
        """
        self._p = p
        self._pb = pb
        self._maxNodes = maxNodes
        super(ForestFire, self).__init__(parent, window, draw)
        
    def _generateInitialGraphs(self):
        self._graph = MyGraph(self, fig=self._fig, xRange = self._getXRange(), yRange=self._getYRange())
        return self._graph,
        
    def startAlgorithm(self, draw = False):
        self._graph.clearAll()
        self._graph.add_node(0)
        self._algoPhase = 0
        self._v = None # new node
        self._visited = None # set of already visited nodes
        self._fireStack = []
        if draw:
            self.setStatusText("Start algorithm, p=%.2f, pb=%.2f" % (self._p, self._pb))
            self.clear()
            self.draw()
        super(ForestFire, self).startAlgorithm(draw)
        
    def getColorSchemes(self):
        return [("Uncolored", lambda _n : 0)]
        
    def _graphRange(self):
        raise NotImplementedError("Implement in subclass")
        
    def _shouldConnect(self):
        raise NotImplementedError("Implement in subclass")
    
    def _relayoutGraph(self, animate=False):
        if animate:
            self._graph.relayout()
        else:
            self._graph.setPositions(None)

    class FireNode(object):
        def __init__(self, w, graph, p, pb, visited):
            self._w = w # ambassador node
            self._ol = [] # chosen out-links
            self._il = [] # chhosen in-links
            self._graph = graph
            visited.add(self._w)
            
            # number of links to form
            if p >= 1:
                # form as many connections as possible
                self._x = sys.maxint
            else:
                self._x = numpy.random.binomial(2./(1. - p), 0.5)
                
            # probability to choose out-links
            
            outLinks = set(self._graph.neighbors(self._w)) - visited
            inLinks = set(self._graph.predecessors(self._w)) - visited
            
            r = float(pb) / p
            numOut = 1. / (1 + r) * self._x
            numIn = int(round(r * numOut))
            numOut = int(round(numOut))
            
            self._chooseLinks(numOut, outLinks, self._ol, visited)
            self._chooseLinks(numIn, inLinks, self._il, visited)
                     
        def _chooseLinks(self, numLinks, linkSource, linkTarget, visited):
            sample = min(len(linkSource), numLinks)
            if sample == 0:
                return 
            chosen = random.sample(linkSource, sample)
            linkSource.difference_update(chosen)
            
            linkTarget.extend(chosen)
            visited.update(chosen)
                        
        def peek(self):
            if self._ol:
                return self._w, self._ol[-1]
            elif self._il:
                return self._il[-1], self._w
            
        def pop(self):
            if self._ol:
                return self._w, self._ol.pop()
            elif self._il:
                return self._il.pop(), self._w
    
        def hasOutLinks(self):
            return len(self._ol) > 0
        
        def hasInLinks(self):
            return len(self._il) > 0
    
        def empty(self):
            return not self._ol and not self._il
    
    CNC = 0.1 # Current node color
    ANC = 0.5 # Ambassador node color
    
    def _continueBurning(self, target, draw = False):            
            # continue burning along new connection
            newNode = self.FireNode(target, self._graph, self._p, self._pb, self._visited)
            self._x = newNode._x
            self._fireStack.append(newNode)
            if newNode.empty():
                # track back to last ambassador with new connections
                self._algoPhase = 3
            elif draw:
                # we can further burn down this path
                if newNode.hasOutLinks():
                    self._algoPhase = 2
                else:
                    self._algoPhase = 3
            else:
                # skip phase 2 and 3 if not drawing
                self._algoPhase = 4
    
    def step(self, draw=False):
        super(ForestFire, self).step(draw)
        
        res = None
        if self._algoPhase == -1: # short resting phase, look at your nice new graph!
            if draw:
                self.clear()
                self.showStats()
            self._algoPhase = 0
            res = True
        elif self._algoPhase == 0: # Add new node
            if self._graph.number_of_nodes() >= self._maxNodes:
                # maximum number of levels reached
                raise StopIteration("Algorithm finished.")
    
            # insert new node
            self._v = self._graph.number_of_nodes()
            self._visited = set()
            self._visited.add(self._v) # don't select in-link to myself
            
            if draw:
                self.clear()
                self._graph.insertNode(self._v, highlight=draw, color = self.CNC, animate=True, relayout=True)
                draw = False
                self.setStatusText("Add new vertex")
            else:
                self._graph.add_node(self._v)
           
            self._algoPhase = 1
        elif self._algoPhase == 1: # choose and connect ambassador node
            # choose ambassador node w
            w = random.randint(0, self._graph.number_of_nodes() - 2)
            
            if draw:
                self.clear(uncolor=False)
                self._graph.highlightNode(w)
                self._graph.colorNode(w, self.ANC)
                self._graph.connectNodes(self._v, w, highlight=draw)
                self.setStatusText("Choose entrance vertex")
            else:
                self._graph.add_edge(self._v, w)
            
            self._continueBurning(w, draw)
        elif self._algoPhase in (2, 3): # color chosen links
            curNode = self._fireStack[-1]
            if draw:
                self.clear(uncolor=False)
                for n in curNode._ol:
                    self._graph.colorEdge(curNode._w, n, 'c', width=MyGraph.ELW * 2)
                for n in curNode._il:
                    self._graph.colorEdge(n, curNode._w, 'g', width=MyGraph.ELW * 2)
                self.setStatusText("x: %d -> %d out-links, %d in-links" % (self._x, len(curNode._ol), len(curNode._il)))
            if curNode.empty():
                self._algoPhase = 6
            else:
                self._algoPhase = 4
        elif self._algoPhase == 4: # color next link to burn
            curNode = self._fireStack[-1]
            if draw:
                self.clear(uncolor=False)
                self._graph.highlightEdge(*curNode.peek())
                self.setStatusText("Choose next link to burn through")
            self._algoPhase = 5
        elif self._algoPhase == 5: # burn through link
            curNode = self._fireStack[-1]
            es, ee = curNode.pop()
            target = es if es != curNode._w else ee
            if draw:
                self.clear(uncolor=False)
                self._graph.connectNodes(self._v, target, highlight=True)
                self._graph.dehighlightEdge(es, ee)
                # moving to a new ambassador node, dehighlight old one
                self._graph.dehighlightNode(curNode._w)
                self._graph.colorNode(curNode._w)
                
                # highlight new ambassador node, even if there are no further connections from here
                self._graph.highlightNode(target)
                self._graph.colorNode(target, self.ANC)
                self.setStatusText("Burn through link")
            else:
                self._graph.add_edge(self._v, target)

            self._continueBurning(target, draw)
        elif self._algoPhase == 6: # back track
            curNode = self._fireStack[-1]
            # curNode should be empty
            self._fireStack.pop()

            if draw:
                self.setStatusText("Move back to last vertex with links to burn through")
                self.clear(uncolor=False)
                # moving to a new ambassador node, dehighlight oldone
                self._graph.dehighlightNode(curNode._w)
                self._graph.colorNode(curNode._w)
            
            # check previous node
            if self._fireStack:
                curNode = self._fireStack[-1]
                # try continuing along this branch
                if draw:
                    self._graph.highlightNode(curNode._w)
                    self._graph.colorNode(curNode._w, self.ANC)
                
                if curNode.empty():
                    # track back to last ambassador with new connections
                    self._algoPhase = 6
                else:
                    # we can further burn down this path
                    self._algoPhase = 4
            else:                
                # burning ceased completely, insert new node
                self._algoPhase = -1
        if draw:
            self.draw()
        return res
