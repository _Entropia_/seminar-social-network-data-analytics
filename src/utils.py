import networkx as nx
import math

def outDegree(graph):            
    # measure out-degree
    degSum = 0
    count = 0
    for _node, degree in graph.out_degree_iter():
        degSum += degree
        count += 1
        
    if count == 0:
        return float('nan')
    else:                        
        return float(degSum) / count

def undirLengths(graph):               
    # measure diameter
    undir = nx.Graph()
    undir.add_nodes_from(graph)
    undir.add_edges_from(graph.edges()) 
    
    allLengths = []
    for v in undir.nodes():
        lengths = nx.single_source_shortest_path_length(undir, v)
        allLengths += lengths.values()
        allLengths += [float('inf')] * (undir.number_of_nodes() - len(lengths))

    allLengths.sort()
    return allLengths

def diameter(allLengths):
    return allLengths[-1]

def effectiveDiameter(allLengths): 
    k = (len(allLengths) - 1) * 0.9
    f = math.floor(k)
    c = math.ceil(k)
    if f == c:
        return allLengths[int(k)]
    else:
        d0 = allLengths[int(f)] * (c - k)
        d1 = allLengths[int(c)] * (k - f)
        return d0 + d1
