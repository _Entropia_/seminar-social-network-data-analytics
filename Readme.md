# Seminar Social Network Data Analytics

This Git repository contains all the stuff I collected and produced for my topic im above seminar. It contains the graph algorithm and visualization code as well as my presentation slides. As you are probably most interested in the source code, here is a short introduction.

# Requirements

The code should run on Python 2.7 (maybe 2.6) and requires the following libraries:

* [networkx](http://networkx.github.io) (Graph structure and basic algorithms)
* [matplotlib](http://matplotlib.org) (Graph drawing, animation)
* [numpy](http://www.numpy.org) (Data structures)
* [PyGraphviz](http://pygraphviz.github.io) (Graph layout)
* [PyQt4](http://www.numpy.org) (GUI)

# Getting this thing to run

... basically means to install all the above libraries, plus one important thing:

## Patching networkx

Unfortunately, networkx is not meant to support animation. Furthermore, the drawing of directed edges with arrows is awful. Hence, I had to fiddle with the networkx source code.

The changes affect only one file, **networkx/drawing/nx_pylab.py**. I added a patch for this file as **src/nx_pylab.patch**. Apply it by switching to the **networkx/drawing** directory and typing

    patch < [path/to]/nx_pylab.patch
 
The patch is based on **networkx 1.8.1**; I cannot guarantee that it will work for other versions.
 
## Running the code

If everything is installed and the python path is set up correctly, running the code is as easy as typing

    python main.py -f #Forest Fire simulation
    python main.py --help #usage page

# Structure

Here is a short introduction in the most important files and classes.

## main.py

Contains the main routine, some test functions as well as the command line parser.

## graph.py

Contains a networkx ```Digraph``` subclass that supports advanced coloring, highlighting and animation functionalities.
For drawing and animation purpuses, some methors are provided that replace the original ```Digraph``` methods, e.g.:

* ```insertNode``` replaces ```Digraph.add_node```
* ```connectNodes``` replaces ```Digraph.add_vertex```

## algo_base.py

Contains the ```Algorithm``` class, that can be subclassed to implement step-by-step, animated graph algorithms.

## Other files

The other files contain are merely utility functions and specific algorithm implementations.
