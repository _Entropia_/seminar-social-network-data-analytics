\documentclass[handout]{beamer}

\usetheme{CambridgeUS}
\usecolortheme{seahorse}
\useinnertheme{circles}

\usepackage{etex}
\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage{tabulary}
\usepackage{booktabs}
\usepackage[cm]{sfmath}
\usepackage{pgfplots}
\usepackage[binary-units = true]{siunitx}
\usepackage{tikz}
\usepackage{etoolbox}
\usepackage{calc}
\usepackage[disable]{todonotes}
\presetkeys{todonotes}{inline}{}

\usetikzlibrary{shapes,arrows,positioning,fit,patterns}

\input{macros}
\input{plot_macros}
\newcommand{\curtitle}{}

\begin{document}

\title{Graphs over Time}
\subtitle{Seminar Social Network Data Analytics}
\author{Cornelius Ratsch}
%\institute{Heidelberg University}
%\date{April 12, 2012}

%\begin{frame}
%  \titlepage
%\end{frame}
\frame[plain]{\maketitle}

\section*{Motivation}

\begin{frame}{Motivation}
\begin{itemize}
\item Graphs are everywhere in computer science:
\begin{itemize}
\item Computer networks
\item Citation networks
\item Social networks
\item etc.
\end{itemize}
\item Real graphs are huge and thus, difficult to obtain and handle
\item[$\Rightarrow$] Generate artificial graphs
\item What are the properties of real graphs?
\item How do they grow and evolve over time?
\end{itemize}
\end{frame}

\section*{Introduction}

\subsection*{Fundamentals}

\newcommand{\examplegraph}{
\begin{tikzpicture}
  [scale=.8,auto=left,every node/.style={circle,fill=blue!20},line width=1]
  \node (n1) at (0,0) {1};
  \node (n2) at (2,0) {2};
  \node (n3) at (2,2) {3};
  \node (n4) at (0,2) {4};

  \foreach \from/\to in {n1/n2,n2/n4,n1/n4,n2/n3}
    \draw (\from) -- (\to);
    
  \node (n1c) at (5,0) {1};
  \node (n2c) at (7,0) {2};
  \node (n3c) at (7,2) {3};
  \node (n4c) at (5,2) {4};
  
  \foreach \from/\to in {n1c/n2c,n1c/n3c,n1c/n4c,n2c/n3c,n2c/n4c,n3c/n4c}
    \draw (\from) -- (\to);
   
  \node (n1d) at (10,0) {1};
  \node (n2d) at (12,0) {2};
  \node (n3d) at (12,2) {3};
  \node (n4d) at (10,2) {4};
  
  \foreach \from/\to in {n1d/n2d,n4d/n2d,n3d/n4d,n4d/n3d,n2d/n3d}
    \draw [->] (\from) -- (\to);
    
\end{tikzpicture}
}

\begin{frame}{Fundamentals}
Graph:
\begin{itemize}
\item Tuple $G=(V, E)$ of vertices (\emph{nodes}) $V$ and edges $E$
\item An edge $e = (v, w), v, w \in V$ is a pair of vertices
\item In a simple (undirected) graph, $e = (v, w)$ implies $e = (w, v)$
\item \emph{Complete Graph}: Any pair of vertices is connected
\end{itemize}

Directed Graph (\emph{Digraph}):
\begin{itemize}
\item Edges have an explicit direction
\item In the remainder, all graphs will be directed graphs
\end{itemize}

\centering
\examplegraph

%\todo{Introduction of the basic notation
%((Di-)Graphs, Scale-Free Graphs / Preferential Attachment, Power Law, Self-Similarity,
%whatever comes to my mind, with examples of course)}
\end{frame}

\begin{frame}{Fundamentals -- Graph Properties}
\begin{itemize}
\item \emph{Degree} of a vertex $v$: Number of edges connected to $v$
\begin{itemize}
\item For Digraphs: Distinct \emph{in-degree} and \emph{out-degree}
\end{itemize}
\item \emph{Path}: Sequence of pairwise connected vertices $(v_1, \dots, v_n)$
\item \emph{Distance} between $v$ and $w$: Length of shortest path $v, v_2, ..., v_n, w$
\item \emph{Diameter} of $G$: Maximum distance between any two vertices in $G$
\item \emph{Effective diameter}: 90\textsuperscript{th} percentile distance
\end{itemize}

\centering
\examplegraph
\end{frame}

\begin{frame}{Fundamentals -- Power Law}
\begin{itemize}
\item Relation $a \sim b^e$ is called \emph{power law}
\item \emph{Example}: $|E| \sim |V|^2$ in dense graphs
\end{itemize}
\end{frame}

%\begin{frame}<handout:0 beamer:0>{Fundamentals -- Self-Similar Graphs}
%\begin{itemize}
%\item The whole graph is similar to parts of itself
%\item Social networks contain self similar structures
%\item \emph{Example}: Communities within communities
%\end{itemize}
%\end{frame}

%TODO preferential attachment model?

\subsection*{}
\begin{frame}{Outline}
\tableofcontents
\end{frame}

\section{Empirical Observations}
\frame{\sectionpage}

%\subsection*{Graph Properties}
\begin{frame}{Evolution of Graph Properties}
How do graph properties change as typical graphs grow?
\begin{itemize}
\item Vertex Degree
\begin{itemize}
\item<2-> Assumed to be constant on the average ($E \sim V$)
\item<2-> Observed: Densification power law ($E \sim V^a, ~a > 1$)
\end{itemize}
%\item vertex-to-vertex-distance
\item Diameter (maximum vertex-to-vertex distance)
\begin{itemize}
\item<2-> Assumed to grow slowly
\item<2-> Observed: Shrinking effective diameter (90th percentile distance)
\end{itemize}
\item<3> What causes this behavior?
\item<3> How can it be modeled to generate graph sequences with identical behavior?
\end{itemize}
\end{frame}

\begin{frame}{Evolution of Graph Properties -- Example}
Citation graph from arXiv
\begin{itemize}
\item Data from 1993-2003
\item \num{29555} papers, \num{352807} edges
\item Contains citations to papers outside the data set
\end{itemize}
\end{frame}

\subsection{Densification Power Law}

\begin{frame}{Densification Power Law}
\centering
\includegraphics[width=\textheight]{fig/densification1}
\end{frame}

\begin{frame}<handout:0>{Densification Power Law}
\centering
\includegraphics[width=\textheight]{fig/powerlaws3}
\end{frame}

\begin{frame}{Densification Power Law}
\begin{itemize}
\item Line in log-log-Plot:
\begin{align*}
\log y &= a \log x + c\\
y &= e^{a \log x + c}\\
y &= e^c \cdot x^a\\
|E| &= const \cdot |V|^a
\end{align*}
\item[$\Rightarrow$] Power law
\end{itemize}
\end{frame}

\begin{frame}{Densification Power Law}
\centering
\includegraphics[width=\textheight]{fig/powerlaws1}
\end{frame}

\subsection{Shrinking Diameters}

\begin{frame}{Shrinking Diameters}
\centering
\includegraphics[width=\textheight]{fig/shrinking1}
\end{frame}

\begin{frame}{Observations}
\begin{itemize}
\item Over time, the observed graphs seem to
\begin{itemize}
\item densify (growing average vertex degree), and
\item shrink (effective diameter)
\end{itemize}
\item What are the reasons?
\item How can this behavior be reproduced?
\end{itemize}
\end{frame}

\section{Generative Models}
\frame{\sectionpage}

\begin{frame}{Generative Models}
\begin{itemize}
\item Models generate directed graphs
\item No coordination -- vertices ``choose'' their own out-links
\item \emph{Goal}: Densification power law, shrinking diameter
\end{itemize}
\end{frame}

\subsection{Community Guided Attachment}

\begin{frame}{Community Guided Attachment}
\begin{itemize}
\item \emph{Goal}: Find model that incorporates the densification behavior
\begin{itemize}
\item Without coordination
\item From intrinsic features of the generation process
\end{itemize}
\item \emph{Idea}: Nested communities
\end{itemize}
\end{frame}

\begin{frame}{Community Guided Attachment -- Basic Version}
\begin{itemize}
\item Community structure represented as balanced tree with constant fanout
\item Leaves of the tree correspond to vertices in the graph
\item Start with unconnected graph
\item Connect any two vertices $v, w \in V$ with probability: $$p(v, w) = c^{-\frac{1}{2}*tree\_dist(v, w)}$$
\item $c \ge 1$ is the \emph{difficulty constant}:
\begin{itemize}
\item $c = 1 \leadsto |E| = |V|\cdot(|V|-1) \sim |V|^2$ (complete graph)
\item $c < fanout \leadsto |E| \sim |V|^{e}$ with exponent $1 < e \le 2$
\end{itemize}
\end{itemize}
\end{frame}

\renewcommand{\curtitle}{Community Guided Attachment -- Basic Version}

\newcommand{\exfrb}[3]{
	\begin{frame}#1{\curtitle}
	\centering
	\includegraphics[width=\textwidth]{fig/#2}

	\raggedright
	#3
	\end{frame}
}

\newcommand{\exfra}[2]{
	\exfrb{}{#1}{#2}
}

\newcommand{\exfr}[2]{
	\exfrb{<handout>}{#1}{#2}
}

\exfra{b00}{
\emph{Left}: community tree, \emph{Right}: generated graph\\
Leaves in community tree correspond to vertices in graph
}

\exfra{b01}{
Difficulty constant: $d=2$\\
Choose two vertices $v$ (green), and $w$ (blue)\\
$dist(v, w)=4 \Rightarrow$ probability to connect: $0.25 \Rightarrow$ not connected
}

\exfra{b02}{
Difficulty constant: $d=2$\\
Choose two vertices $v$ (green), and $w$ (yellow)\\
$dist(v, w)=2 \Rightarrow$ probability to connect: $0.5 \Rightarrow$ connected
}

\exfra{b03}{
Repeat previous steps for remaining pairs of vertices
}

\begin{frame}{\curtitle}
\degdiamplot{bcgalegend}{
	\degdiamplotlinesx{bcga}{1.5,2,3}{$d=1.5$,$d=2$,$d=3$}{Degree}
}{
	\degdiamplotlinesx{bcga}{1.5,2,3}{$d=1.5$,$d=2$,$d=3$}{Eff.Diameter}
}
\end{frame}

\begin{frame}{\curtitle}
\begin{itemize}
\item Produces self-similar structure
\item Produces power-law edge distribution
\item Works on a graph with a constant number of vertices $\Rightarrow$ no dynamic 
growth
\item[$\Rightarrow$] Dynamic variant
\end{itemize}
\end{frame}

\renewcommand{\curtitle}{Community Guided Attachment -- Dynamic Version}

\begin{frame}{\curtitle}
\begin{itemize}
\item \emph{Goal}: Add dynamic growth to the model
\item \emph{Idea}:
\begin{itemize}
\item Expand the nested community structure as vertices are added
\item Vertices only form out-links as they are added \todo{example: citation networks}
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{\curtitle}
\begin{itemize}
\item Now, all vertices of the community tree correspond to vertices in the graph
\item Start with a graph containing only one vertex
\item During each time step $t$:
\begin{enumerate}
\item A new level is added to the tree $\Rightarrow fanout^t$ new vertices
\item New vertices form out-links like before: $$p(v, w) = c^{-\frac{1}{2}*tree\_dist(v, w)}$$
\end{enumerate}
\end{itemize}
\end{frame}

\exfr{d00}{
\emph{Left}: community tree, \emph{Right}: generated graph\\
Begin with graph containing only one vertex
}
\exfr{d01}{
Insert next level of vertices into community tree and graph
}
\exfr{d02}{
Difficulty constant: $d=2$\\
Choose new vertex $v$ (blue) and random vertex $w$ (yellow)\\
$dist(v, w)=2 \Rightarrow$ probability to connect: $0.5 \Rightarrow$ connected
}
\exfr{d03}{
Difficulty constant: $d=2$\\
Choose new vertex $v$ (magenta) and random vertex $w$ (white)\\
$dist(v, w)=1 \Rightarrow$ probability to connect: $\frac{1}{\sqrt 2} \approx 0.7 \Rightarrow$ connected
}
\exfr{d04}{
Continue for remaining pairs of (\emph{new vertex}, \emph{random vertex}) 
}
\exfr{d05}{
Insert next level of vertices into community tree and graph
}
\exfr{d06}{
Difficulty constant: $d=2$\\
Choose new vertex $v$ (orange), and random vertex $w$ (blue)\\
$dist(v, w)=3 \Rightarrow$ probability to connect: $\frac{1}{\sqrt {2^3}} \approx 0.35 \Rightarrow$ not connected
}
\exfr{d07}{
Difficulty constant: $d=2$\\
Choose new vertex $v$ (violet) and random vertex $w$ (magenta)\\
$dist(v, w)=1 \Rightarrow$ probability to connect: $\frac{1}{\sqrt 2} \approx 0.8 \Rightarrow$ connected
}
\exfr{d08}{
Continue for remaining pairs of (\emph{new vertex}, \emph{random vertex}) 
}

\begin{frame}<handout:0>{\curtitle}
Live demonstration
\end{frame}

\begin{frame}<beamer:0>{\curtitle}
\degdiamplot{dcgalegend}{
	\degdiamplotlines{dcga_}{1.5}{Degree}
	\degdiamplotlines{dcga}{2,3}{Degree}
}{
	\degdiamplotlines{dcga_}{1.5}{Eff.Diameter}
	\degdiamplotlines{dcga}{2,3}{Eff.Diameter}
}
\end{frame}

\begin{frame}{\curtitle}
$$p(v, w) = c^{-\frac{1}{2}*tree\_dist(v, w)}$$

Properties of the Dynamic Community Guided Attachment Model:
\begin{itemize}
\item Densification power law with exponent controlled by $c$ 
\item Dynamic growth of graph
\item Only new vertices form out-links $\Rightarrow$ no complete graph possible
\end{itemize}
\end{frame}

\renewcommand{\curtitle}{Forest Fire Model -- Example}

\subsection{Forest Fire Model}
\begin{frame}{Forest Fire Model}
\begin{itemize}
\item Community Guided Attachment models do not incorporate shrinking diameters
\item \emph{Goal}: Densification power law \emph{and} shrinking diameters
\item \emph{Ideas}: 
\begin{itemize}
\item ``Rich get richer'' (heavy-tailed in-degrees)
\item ``Copy'' edges from neighbors (community structure)
\item ``Some flavor'' of CGA (densification power law)
\item ``Unknown ingredient'' (shrinking diameter)
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Forest Fire Model}
\begin{itemize}
\item \emph{Input}:
\begin{itemize}
\item Forward burning probability $p \in [0, 1]$
\item Backward burning ratio $r \in [0, 1]$
\end{itemize}
\item Start with a graph containing only one vertex
\item When a new vertex $v$ is added:
\begin{enumerate}
\item $v$ randomly chooses entrance vertex $w$ and connects to it
\item $v$ chooses on the average $x=\frac{1}{1-p}$ edges incident to $w$\\
$x$ is binomially distributed $\Rightarrow x \in \left[0, \frac{2}{1-p}\right]$
\item In-links are chosen $r$ times as frequently as out-links
\item $v$ connects (\emph{burns through}) to other ends
\item Steps 2 -- 4 are recursively applied on other ends
\end{enumerate}
\end{itemize}
\end{frame}

\begin{frame}<handout:0>{Forest Fire Model}
Live demonstration
\end{frame}

\exfr{f00}{
We omit the first steps and begin with a graph that already contains 4 vertices.\\
Insert new vertex $v=v_4$ (green)
}
\exfr{f01}{
Choose entrance vertex $v_0$ (red) and connect to it
}
\exfr{f02}{
$p=0.25, r=0.2$\\
Randomize $x \leadsto$ $x=1 \Rightarrow$ select 1 in-link of $v_0$
}
\exfr{f03}{
Select next edge to ``burn through'': $(v_0, v_2)$
}
\exfr{f04}{
Connect $v$ to other side of selected edge: $v_2$\\
Continue burning at $v_2$
}
\exfr{f05}{
$p=0.25, r=0.2$\\
Randomize $x \leadsto$ $x=1 \Rightarrow$ select 1 out-link of $v_2$
}
\exfr{f06}{
Select next edge to ``burn through'': $(v_2, v_1)$
}
\exfr{f07}{
Connect $v$ to other side of selected edge: $v_1$\\
$v_1$ is not connected to vertices that weren't already visited\\
$\Rightarrow$ go back to last vertex with edges to burn
}
\exfr{f08}{
No more edges selected to burn $\Rightarrow$ fire burnt out\\
New vertex $v_4$ formed 3 out-links
}

\begin{frame}{Forest Fire Model}
How are the ideas realized?
\begin{itemize}
\item ``Rich get richer'' (heavy-tailed in-degrees)
\begin{itemize}
\item[$\rightarrow$] Highly linked vertices can easily be reached
\end{itemize}
\item ``Copy'' edges of neighbors (community structure)
\begin{itemize}
\item[$\rightarrow$] Entrance node is neighbor
\end{itemize}
\item ``Some flavor'' of CGA (densification power law)
\begin{itemize}
\item[$\rightarrow$] Entrance vertices correspond to parent vertices in CGA $\Rightarrow$ community structure
\end{itemize}
\item ``Unknown ingredient'' (shrinking diameter)
\begin{itemize}
\item[$\rightarrow$] Some vertices produce a very large number of out-links $\Rightarrow$ bridges between sparsely connected parts
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Forest Fire Model -- Graph Properties}
\degdiamplot{fflegend}{
	\degdiamplotlinesx{ff_}
		{(0.37;0.32)}
		{{$p=0.37, r=0.32$}}
		{Degree}
}{
	\degdiamplotlinesx{ff_}
		{(0.37;0.32)}
		{{$p=0.37, r=0.32$}}
		{Eff.Diameter}
}
\end{frame}

\section{Conclusions}
\frame{\sectionpage}

\begin{frame}{Conclusions}
\begin{itemize}
\item Graphs seem to densify and shrink over time
\item Generative Models were introduced to reproduce this behavior:
\begin{itemize}
\item Community Guided Attachment (densification)
\item Forest Fire Model (densification and shrinking diameter)
\end{itemize}
\item Observations are very much based on citation networks
\end{itemize}

\todo{comparison with what other researchers observed?}
\end{frame}

\begin{frame}{Conclusions}
Generative Models:
\begin{itemize}
\item[$+$] Simple models, easy to implement
\item[$+$] The models are well motivated for the given use case
\item[$-$] Restricted to specific assumptions:
\begin{itemize}
\item Vertices only form out-links as they are added
\item No vertices or edges can be removed
\end{itemize}
\item[$-$] Forest Fire Model not sufficiently examined:
\begin{itemize}
\item[$\rightarrow$] What exactly is the ``unknown ingredient'', that leads to shrinking diameters?
\end{itemize}
\end{itemize}

\end{frame}

\begin{frame}[plain]{Thank you for listening}
Questions?
\end{frame}

\end{document}
