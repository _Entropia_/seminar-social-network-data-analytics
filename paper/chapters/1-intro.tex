\section{Introduction}\label{sec:introduction}

This work is based on the article ``Graphs Over Time'', by \citeauthor{Leskovec2005} 
\cite{Leskovec2005}. 
In this section, the field of generative graph models is motivated and 
fundamental concepts are introduced. Then, a detailed overview of related work 
is given and graph models related to those introduced by \citeauthor{Leskovec2005}
are described.

\subsection{Motivation}

In information science, graphs are used to describe a variety of real world 
structures, like the internet, networks of social interaction or protein structures.
Hence, there has been a lot of work on describing properties that these graphs examine
as well as on how to generate artificial graphs that match the real ones in terms
of these properties.

If a graph generation model is able to 
reproduce the properties of real world graphs, this model can serve several 
purposes. On the one hand, it allows to simulate the behavior of algorithms on a 
real world graph by applying it on a smaller, artificial graph, either to reduce the
run time or because the real data is not available. On the other 
hand, one can make simulations on generated graphs that are bigger than the 
modeled real world graph, to predict the performance of the algorithm in the 
future \cite{Kleinberg1999}.

A generative model that fits the modeled graph well might serve another important 
purpose. If the model is parsimonious with intuitive parameters, those parameters can be 
used to describe the graph in a very compressed form or to measure the similarity
between two graphs. Further, the 
model can support the understanding of how real world graphs develop and what 
processes lead to certain patterns, like the vertex degree 
distribution, or community structures \cite{Akoglu2009}.

\subsection{Fundamentals}

In the following, the basic notation of a graph as well as some popular graph 
properties are introduced.

\subsubsection{Definitions}

\begin{definition}[Graph]
A \emph{graph} is a tuple $G = (V, E)$ of \emph{vertices} $V$ and \emph{edges} $E$.
An \emph{edge} $e$ is a pair of vertices $e = (v, w), ~v, w \in V$.
Graphs can either be
\begin{itemize}
\item \emph{undirected}, \ie $(v, w) \in E \Rightarrow (w, v) \in E$, or
\item \emph{directed}, where edges have an explicit direction.
\end{itemize}
A graph $G = (V, E)$ is \emph{complete}, if for each pair of vertices $v, w \in 
V$, there is an edge $(v, w) \in E$.
\end{definition}

\begin{definition}[Path]
A sequence of pair-wise connected vertices $v_1, \dots, v_n$ is called a \emph{path}.
\end{definition}

\subsubsection{Graph Properties}
For a graph $G = (V, E)$, the following properties can be observed:
\begin{itemize}
\item The \emph{degree} of a vertex is the number of edges connected (\emph{incident}) to $v$.
For directed graphs, one can distinguish between
\begin{itemize}
\item \emph{in-degree}, \ie the number of edges $e = (w, v) \in E, w \in V$, and 
\item \emph{out-degree}, \ie the number of edges $e = (v, w) \in E, w \in V$.
\end{itemize}
\item The \emph{distance} between two vertices $v$ and $w$ is the 
length of the shortest path starting at $v$ and ending at $w$.
\item The \emph{diameter} of $G$ is the maximum distance 
between any two vertices $v, w \in V$.
\item The \emph{effective diameter} of $G$ is the 90\textsuperscript{th}
percentile of the distances between all vertices $v, w \in V$.
\item If the vertices within a certain subgraph of $G$ are ``closer'' to each other than to the other
vertices of $G$, the subgraph is called a \emph{community}.
\item If $G$ features a fractal-like structure, \ie $G$ is similar to subgraphs 
of $G$, it is called \emph{self-similar}.
\end{itemize}

\subsubsection{Scale-Free Graphs}
When observing the distribution of vertex degrees in real world graphs, like 
citation networks or the World Wide Web, one will notice that there are few 
vertices with a high degree and a majority of vertices with a very small 
degree. To be more precise, the probability $P$ that a vertex has a certain degree $d$
typically follows the law $P(d) \propto d^{\alpha}$ with a constant exponent $\alpha < 0$ \cite{Newman2003}.
Such a relation is called a \emph{power law}.

One property of a power law is that it is \emph{scale-free}. In above case, if 
you scale the degree by a constant factor $k$, the probability is scaled by a 
factor of $k^{\alpha}$.

If the vertex degree distribution of a graph follows a power law, as above, the 
graph is called \emph{scale-free}.

\subsection{Background and Related Work}

There has been a lot of work on graphs and their structures. In the following, 
some of the work covering properties observed in real world graphs 
(\refsec{staticprops}), how these properties evolve over time 
(\refsec{dynamicprops}), and how these properties can be modeled 
(\refsec{generativemodels}), is outlined. The article of \citeauthor{Leskovec2005} 
\cite{Leskovec2005} is fit into the related 
work.

\subsubsection{Properties of Static Graphs}\label{sec:staticprops}

There has been a lot of work covering different properties of real world graphs.

One of the best studied aspects is the \emph{vertex degree distribution} 
\cite{Newman2003}. Power-law degree distributions of the form $P(d) \propto d^{\alpha}$, $\alpha < 0,$ 
have been observed in many real world graphs, such as the the World Wide Web \cite{Kleinberg1999}
or the internet \cite{Faloutsos1999}.
Other examples are phone call graphs, click stream data or a who-trusts-whom social
network \cite{Leskovec2005}.

\emph{Communities} are another feature observable in many real world graphs. 
Intuitively, they can be found in social network graphs 
\cite{Moody2001, Girvan2002}, but they were also observed in the World Wide Web \cite{Flake2000, Flake2002} 
and in biological networks \cite{Girvan2002}.
%A typical measurement to detect community structures in graphs is the clustering 
%coefficient \cite{Newman2003, DeepayanChakrabarti2006}.
In a more recent work, \citeauthor{Leskovec2008} \cite{Leskovec2008} observe the community structures in large 
real-world social and information networks and conclude that well-separated 
communities exists only with a small number of $\approx 100$ vertices. Larger 
communities tend to ``blend into'' the remaining graph.

\emph{Self-similarity} (\ie fractal structures) can be observed in several kinds of 
networks, \eg ethernet traffic \cite{Leland1994}, the World Wide Web 
\cite{Crovella1997}, file systems \cite{Gribble1998} or disk-level I/O requests
\cite{Gomez1999}. A more recent work by \citeauthor{McGlohon2008} \cite{McGlohon2008} discover 
self-similar structures in a variety of graph data sets, \eg blog posts, 
citation networks and autonomous systems.

Another popular concept is called the ``six degrees of separation''. Its origin is 
a social experiment by Milgram in 1967 \cite{Milgram1967} that leads to the 
conclusion that any two people in the United States are connected by a path with 
only 5 intermediaries.
This \emph{small world phenomenon}, \ie rather small diameters in huge graphs, can
also be observed in the World Wide Web \cite{Albert1999} and was later extended 
to random scale-free graphs \cite{Chung2003}.
A recent work by Backstrom \citeauthor{Backstrom2012} \cite{Backstrom2012} reveals that the average number of 
intermediaries in the Facebook network is only $\approx 3.74$.

The above is just a subset of laws observable in real world graphs. For 
example, \citeauthor{McGlohon2008} \cite{McGlohon2008} observe additional laws such 
as a \emph{weight power law}, \ie the sum of edge weights is a power of the 
number of edges, or the \emph{snapshot power law}, \ie the sum of edge weights 
attached to a vertex is a power of the vertex's degree.
These laws are not relevant for this work.

\subsubsection{Properties of Dynamic Graphs}\label{sec:dynamicprops}

The previous laws apply to a static snapshot of a graph. There has also been
work on how graphs and their properties evolve over time.

\citeauthor{Leskovec2005} \cite{Leskovec2005} empirically observe that the average vertex 
degree of several real world graphs grows over time, following a power law (see 
\refsec{densification}). Katz \cite{J.SylvanKatz2005} independently observe 
this behavior for citation networks.
Later work suggests that online social network graphs do not follow this pattern 
\cite{Kumar2006, Hu2009}. Here, the density follows a non-monotone function, 
consisting of a fast increase followed by a dip, and finally a slow but steady 
increase.

Another observation by \citeauthor{Leskovec2005} \cite{Leskovec2005} is that the effective diameter constantly shrinks over 
time (see \refsec{shrinking}). A similar effect can be observed in online social 
networks \cite{Kumar2006, Hu2009}. However here, the diameter grows in the phase 
where the density drops, but finally shrinks again.

In a more recent work, \citeauthor{Leskovec2008a} analyze four large online social
networks for their evolution behavior \cite{Leskovec2008a}. They conclude that 
the Preferential Attachment model (see \refsec{preferential-attachment})
does not model the observed behavior properly. They 
describe the the growth of a graph by three basic processes: The \emph{vertex arrival 
process}, \ie the rate of newly created vertices, the \emph{edge initiation 
process}, \ie vertices only creates edges during a certain life-time, and the
\emph{edge destination selection process}, stating that most edges 
close triangles and thus, are local.

\newpage
\subsubsection{Graph Generator Models}\label{sec:generativemodels}

The previous sections describe properties observed in real world graphs. There 
has been a lot of work on how to generate artificial graphs that exhibit 
similar properties.

One of the first approaches is the Erdős-Rényi model \cite{Erdos1960}, also 
known as $G_{n,p}$. It generates a graph with $n$ vertices and any two vertices 
are connected with a probability of $p$.
The Erdős-Rényi model is very simple and does not fit real world graphs, like the
World Wide Web, very well \cite{Kumar2000}. For example, the vertex degrees in $G_{n,p}$ do 
not follow a power law, but rather a Poisson distribution \cite{DeepayanChakrabarti2006}.
Most of the more recent approaches incorporate the power law distribution.
Chakrabarti and Faloutsos \cite{DeepayanChakrabarti2006} give a very 
detailed overview of various graph generator models.

One class of approaches is the class of \emph{Generalized Random Graph Models}.
They extend the Erdős-Rényi 
model to allow arbitrary degree distributions \cite{DeepayanChakrabarti2006}.
These models have the disadvantage of not providing any insight on which 
processes cause the distribution in real world graphs.

Another class of models focuses on the processes that create 
real world graphs. For example, one could model the behavior of users in a 
social network to generate an artificial social network graph. The desired 
properties, such as the power law degree distributions, are intended to follow 
naturally. 
According to Akoglu and Faloutsos \cite{Akoglu2009}, such a model should ideally be
\begin{itemize}[noitemsep]
\item \emph{easy to understand}, and intuitively lead to the desired patterns,
\item \emph{realistic}, \ie produce graphs that obey the discovered laws of real-world 
graphs,
\item \emph{parsimonious}, \ie only require a small number of parameters,
\item \emph{flexible}, \ie generate both weighted and unweighted, directed and undirected, 
as well as unipartite and bipartite graphs, and
\item \emph{fast}, \ie linear with respect to the number of edges.
\end{itemize}

One of the first models in this class is the \emph{Preferential Attachment} model.
It is based on the observation that vertices tend to generate edges to 
highly connected vertices rather than to isolated vertices. This model is further 
described in \refsec{preferential-attachment}.

Another approach is the \emph{Copying Model}. Here, the idea is that in 
the World Wide Web, webpage creators will link other pages within their topic of interest
that are known to them, and will then link to other pages closely related to these 
pages \cite{Kleinberg1999}. To model this behavior, edges are ``copied'' from a 
randomly chosen prototype vertex. See \refsec{copying-model} for details.

\citeauthor{Leskovec2005} \cite{Leskovec2005} propose two approaches that are closely 
related to the Preferential Attachment Model and the Copying Model. They are 
intended to incorporate the densification power law as well as shrinking 
diameters. These models are described in detail in \refsec{models}.

\subsection{Preferential Attachment}\label{sec:preferential-attachment}

The \emph{Preferential Attachment} model described by Barabasi and Albert \cite{Barabasi1999}
generates undirected graphs where vertices enter and form links one at a time. 
The idea of preferential attachment is that a new vertex prefers connections to 
highly connected vertices. The algorithm is described as follows:

The initial graph contains a small number vertices $|V|=m_0$. As a new vertex $v$
enters the graph, it forms a constant number of $m \le m_0$ edges to other
vertices. The probability that an edge $(v, v_i)$ is created depends on the 
degree of $v_i$: $$P(v, v_i) = \frac{\text{degree}(v_i)}{\sum_{j=1}^N \text{degree}(v_j)}$$

The model leads to a vertex degree distribution $P(d) \propto d^{-3}$ \cite{Newman2003}.
This ``rich get richer'' property is widely
considered the probable explanation for the power law degree distribution in real
world graphs \cite{Newman2003}.

\subsection{Copying Model}\label{sec:copying-model}
A model to generate directed, scale-free networks is the one described by \citeauthor{Kleinberg1999}
\cite{Kleinberg1999, Kumar2000}. It is based on copying edges from other 
vertices. The model is motivated by how page creators in the World Wide Web link
their pages to other pages:

\begin{itemize}
\item Some links may be chosen arbitrarily, without regard to the topic of the 
page, but
\item mostly, links will be formed to pages within certain topics of interest to 
the creator.
\end{itemize}

The model is supposed to be rooted in this macro-level process and to not 
incorporate any a priori knowledge of topics. These topics, that can also be 
looked at as communities, should form automatically, as a feature of the model.
In the following the algorithm is described as in \cite{Kumar2000}. The original 
model differs slightly, but was not rigorously analyzed. The Copying Model is
parametrized by
\begin{itemize}
\item the \emph{copying factor} $c \in [0,1]$, and
\item the \emph{out-degree} $d \in \mathbb{N}, d \ge 1$.
\end{itemize}

At each time step, one vertex $v$ is inserted into the graph.
Then, $d$ out-links are chosen as follows:
\begin{itemize}
\item A prototype vertex $p \in V$ is chosen uniformly at random.
\item Each out-link $e_i$ is chosen as follows: With probability $c$, $e_i$ is 
the $i$-th out-link of $p$. With the remaining probability, the link target is 
chosen from $V$ uniformly at random.
\item If $p$ does not have enough out-links, another prototype vertex is 
chosen.
\end{itemize}

The algorithm leads to a vertex degree distribution $P(d) \propto d^\alpha$, $\alpha = 
-\frac{2-c}{1-c}$ \cite{Newman2003}.
Note that if $c = 0.5$, this model leads to the same vertex degree distribution as
the Preferential Attachment model.

As stated before, the model initially was motivated by the link formation in the 
World Wide Web.
\citeauthor{Newman2003} \cite{Newman2003} argues that the copying model might not apply to the 
growth of the World Wide Web, but is of interest in the field of protein 
interaction.
