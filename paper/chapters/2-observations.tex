\section{Observations on Evolving Graphs}\label{sec:observations}

In their article ``Graphs Over Time'' \cite{Leskovec2005}, \citeauthor{Leskovec2005} study the properties
of several real world graphs. Unlike former work, they do not observe a single 
snapshot of a graph, but focus on how graph properties change over time. 
Their observations cover both vertex degree and diameter of the graphs.

\begin{samepage}
The authors study 9 data sets from 4 different sources:
\begin{itemize}
\item Citation graphs from \emph{arXiv}\footnote{arXiv: \href{http://arxiv.org}{http://arxiv.org}}:
\begin{itemize}
\item HEP-TH (high energy physics theory)
\item HEP-PH (high energy physics phenomenology)
\end{itemize}
\item A US patent citation graph
\item A graph of autonomous systems in the internet
\item Bipartite affiliation graphs generated from the arXiv data, consisting of 
papers on the one side and authors on the other side. 5 different graphs of this 
kind are examined.
\end{itemize}
\end{samepage}

All data sets either provide snapshots for different time points or provide 
information such that multiple snapshots can be generated.
The following sections describe the two main results the authors gained from 
studying these data sets: the \emph{densification laws} (\refsec{densification})
and \emph{shrinking diameters} (\refsec{shrinking}). In the remainder of this 
chapter, only the plots of the arXiv data are shown exemplarily. Refer to \cite{Leskovec2005}
for the remaining plots.

\subsection{Densification Laws}\label{sec:densification}

\citeauthor{Leskovec2005} state that prior to their evaluations, the 
``conventional wisdom'' implicated that the average vertex degree in real graphs 
stays constant over time. 
Their evaluations show that this assumption does not apply to their data sets.
\reffig{densificationlaw} shows how the vertex degree changes in the arXiv 
citation graph. In \reffig{deggrow}, one can see that the average vertex 
out-degree grows monotonically.

\reffig{deglaw} shows the relation between the number 
of edges and the number of vertices for different time stamps. Note that no 
vertices disappear from the graph, therefore the graph can only grow. Hence, the 
points in the plot still represent a time-line from left to right.

One can see that on the log-log scale, the points form a line and hence, 
the relation between the number of edges and the number of vertices follows a 
power law with a positive exponent. The diagram also contains the plot of the power
law that approximates the points in the diagram. Here, the relation between the
number of edges $|E|$ and the number of vertices $|V|$ is $|E| \approx 
0.0113\cdot |V|^{1.69}$.

\begin{figure}[ht]
\centering
\begin{subfigure}{.48\textwidth}
\centering
\coordimage{densification}{Year of publication}{Average vertex degree}
\caption{Average vertex degree over time.}
\label{fig:deggrow}
\end{subfigure}
~
\begin{subfigure}{.48\textwidth}
\centering
\coordimage{powerlaw}{Number of vertices}{Number of edges}
\caption{Number of edges per vertex, for different time stamps.}
\label{fig:deglaw}
\end{subfigure}
\caption{The arXiv citation graph becomes denser over time.}
\label{fig:densificationlaw}
\end{figure}

The vertex degree becomes higher and hence, the graph becomes denser.
The power law relation between the number of edges and the number of vertices is 
referred to by the authors as the \emph{Densification Power Law}. 

\subsection{Shrinking Diameters}\label{sec:shrinking}

\citeauthor{Chung2003} \cite{Chung2003} suggest that the diameter of a random scale-free graph 
similar to social networks or citation graphs is of order $\log N$. This suggests 
that as the graph grows and new vertices enter the graph, the diameter slowly grows,
too. The findings of \citeauthor{Leskovec2005} suggest a different behavior.

\reffig{shrinking} shows the effective diameter of the (undirected) arXiv citation graph for different 
time stamps. One can see that the diameter shrinks over the years. Only between 
the first two time stamps, there is an small increase, probably due to 
incomplete data. Especially, there are citations to papers outside the data set. 
To proof that the shrinking behavior is not due to such artifacts, the authors 
extracted subgraphs without the data from the first years and without edges to 
those ``past'' vertices. Since the results are similar to the ones of the 
uncleaned data, the authors conclude that this \emph{Shrinking Diameter} law is 
inherent to the growth process of the graph.

For the case of citation graphs, the authors explain this behavior like this:
New citations act as ``bridges'' between formerly separated areas of the graph. 

\begin{figure}[ht]
\centering
\coordimagex{6cm}{shrinking}{Time [years]}{Effective Diameter}
\caption{The effective diameter of the arXiv citation graph shrinks over time.}
\label{fig:shrinking}
\end{figure}

The previous sections showed the laws \citeauthor{Leskovec2005} observed by studying 
real world graph data. The authors propose two models to generate graphs 
following these laws. These models are described in the following section.
