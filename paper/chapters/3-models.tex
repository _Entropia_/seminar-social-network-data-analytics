\section{Generative Models}\label{sec:models}

The models described in the following sections generate directed graphs. They 
are based on the idea that vertices ``choose'' their out-links without 
coordination. Properties such as a community structure, densification or 
shrinking diameters should be caused by these local decisions.

\subsection{Community Guided Attachment Model}

The first model is intended to provide an intuitive explanation for the 
densification behavior observed \refsec{densification}. It is based on the idea 
that power laws follow from a community hierarchy.

\subsubsection{Basic Version}

The basic version of the Community Guided Attachment model generates graphs of a 
fixed size. It does not generate dynamically growing graphs and is
only intended to provide an analytically provable densification power law in 
terms of the relation between the number of vertices and edges.

The community structure of the vertices is represented by a balanced tree $\Gamma$ with 
constant fanout. The leaves of this tree correspond to the vertices in the 
generated graph $G = (V, E)$.
The model is parametrized by
\begin{samepage}
\begin{itemize}
\item the tree fanout $b$,
\item the tree height $H$, and
\item the \emph{difficulty constant} $c \ge 1$, quantifying the difficulty to form a  
link that crosses communities.
\end{itemize}
\end{samepage}

Note that the number of vertices $|V| = b^H$ is provided implicitly.
Any two vertices $v, w \in V$ are connected with a probability $p(v, w)$, that 
depends on both the difficulty constant $c$ and the distance of $v$ and $w$ in $\Gamma$
(\emph{tree\_dist}).
This probability function is intended to be scale-free, and hence, is defined as
$$p(v, w) = c^{-\frac{1}{2}*\text{tree\_dist}(v, w)}$$

\reffig{bcga-example} shows how edges are created in this model. On the left side of each 
image, the community tree $\Gamma$ is visualized, with the colored leaves 
corresponding to the colored vertices in the generated graph ($G$) on the right side.
Note that the two selected vertices in 
\reffig{b02} are connected, since they are close together in a small community 
and thus, their distance in the community tree is small. Since any pair of 
vertices is considered as a candidate for creating an edge, the runtime 
complexity is in any case $\mathcal{O}(|V|^2)$.

\begin{figure}[ht]
\centering
\begin{subfigure}{\textwidth}
\centering
\includegraphics[width=0.6\textwidth]{fig/b01}
\caption{$v$: green, $w$: blue $\Rightarrow \text{tree\_dist}(v, w) = 4 \Rightarrow p(v, w) = \frac{1}{4}$}
\label{fig:b01}
\end{subfigure}
\\
\begin{subfigure}{\textwidth}
\centering
\includegraphics[width=0.6\textwidth]{fig/b02}
\caption{$v$: green, $w$: yellow $\Rightarrow \text{tree\_dist}(v, w) = 2 \Rightarrow p(v, w) = \frac{1}{2}$}
\label{fig:b02}
\end{subfigure}
\caption{Example of the basic Community Guided Attachment model with $b = 3$, $H = 3$, $c = 
2$, \emph{left}: community tree $\Gamma$, \emph{right}: generated graph $G$.}
\label{fig:bcga-example}
\end{figure}

Intuitively, if $c = 1$, the probability to connect any two vertices is 1 and hence,
this model generates a complete graph. For a greater difficulty constant, the 
authors proof that the average vertex out-degree $\bar d$ depends on $c$ as 
follows:
$$
\bar d = \begin{cases}
|V|^{1 - \log_b(c)} & \text{if } 1 \le c < b\\
log_b(|V|) & \text{if } c = b\\
const & \text{if } c > b
\end{cases}
$$

Since the number of edges in the graph is $|V|\cdot \bar d$, $1 \le c < b$ leads to 
a densification power law $|E| = |V|^{2 - \log_b(c)}$.

\subsubsection{Dynamic Version}

The densification power law is initially based on the observation that the 
average vertex degree grows while the graph grows. The basic model does not 
incorporate dynamic growth. Thus, the authors introduce a dynamic version of 
the model. To accomplish dynamic growth, the vertices of $G$ now correspond to
\emph{all} vertices in 
$\Gamma$, not only the leaves. Then, to add new vertices to $G$, a new level of 
vertices is inserted into $\Gamma$. Hence, at time step $t$, $b^t$ new vertices are 
added to $G$. Further, only new vertices form out-links. The link formation rule 
is identical to the basic version.

The authors state that similar to the basic version, the average vertex degree
depends on $c$ as follows, though they do not provide a proof in their article:
$$
\bar d = \begin{cases}
|V|^{1 - \log_b(c)} & \text{if } 1 \le c < b\\
const & \text{if } c > b
\end{cases}
$$

\subsection{Forest Fire Model}

The Community Guided Attachment model can generate graphs that grow dynamically 
and follow a densification power law. However, the diameter does not shrink. 
Hence, \citeauthor{Leskovec2005} introduce a second graph generation model that 
incorporates both densification power laws and shrinking diameters, as well as 
dynamic growth.

\citeauthor{Leskovec2005} give the following motivation for this model: When an 
author of an article is searching for other articles to cite, the author will 
first look for a related article (the ``entrance'' article). Then, further 
related articles will be taken from the references of the article (``forward 
burning''), or the author searches for articles that cite the entrance article
(``backward burning''). 
The model is parametrized by
\begin{itemize}
\item the \emph{forward burning probability} $p \in [0,1]$, and
\item the \emph{backward burning probability} $p_b \in [0,1]$.
\end{itemize}

Vertices enter the graph one by one. Whenever a new vertex $v$ enters the graph $G = (V, E)$:
\begin{enumerate}
\item An entrance vertex $w \in V$ is chosen uniformly at random and an edge
$(v, w)$ is created.
\item A random number of edges incident to $w$ are chosen to determine new 
out-links of $v$. On the average, $x=\frac{1}{1-p}$ edges are chosen, where 
$x$ is binomially distributed $\Rightarrow x \in \left[0, \frac{2}{1-p}\right]$.
\item While choosing from the incident edges of $w$, in-links $(x, w)$ are chosen
$\frac{p_b}{p}$ times as frequently as out-links $(w, x)$.
\item Out-links from $v$ are created (``burnt through'') to the other ends $x$ of the
selected edges.
\item Steps 2--4 are applied recursively with $x$ as the new entrance vertex, until
the ``fire burns out''. No vertex is visited twice during this process.
\end{enumerate}

\reffig{ff-example} shows an example of the Forest Fore model, with $p = 0.25$, and
$p_b = 0.2$. For illustration purposes, the algorithm starts with a graph that
already contains 4 vertices, 0--3. Newly created edges are printed red.

\begin{enumerate}
\item A new vertex (vertex 4, green) enters the graph and connects 
to a randomly chosen entrance vertex (vertex 0, red).
\item The random variable $x$ decides 
that 2 links should be chosen. These are divided into in- and out-links by the 
ratio $\frac{p_b}{p} = 0.8 \Rightarrow 0.89$ in-links and $1.11$ out-links. The 
article does not state how to determine an integral number of vertices given these 
numbers, I assume that the values are rounded. Since vertex 0 has no out-links, only 
one in-links is selected.
\item The selected in-link is the 
edge $(x=2, w=0)$, thus, a new edge to vertex $x=2$ is created.
\item The algorithm is continued recursively, with vertex 2 as the new entrance 
vertex. Here, only one out-link is left (vertex 0 was already 
visited). This out-link is selected.
\item A new edge to the link target $x = 1$ is created.
\item The recursion ends here, since vertex 1 is not 
connected to any vertex that was not already visited.
\end{enumerate}

\newcommand{\sfig}[2]{
\begin{subfigure}[b]{0.3\textwidth}
\centering
\includegraphics[scale=0.6]{fig/#1}
\caption{#2}
\label{fig:#1}
\end{subfigure}
}

\begin{figure}[ht]
\centering
\sfig{ff01}{Entrance vertex}
\sfig{ff02}{$x = 2 \Rightarrow 1$ in-link}
\sfig{ff03}{Burn through in-link}\\
\sfig{ff04}{$x = 1 \Rightarrow 1$ out-link}
\sfig{ff05}{Burn through out-link}
\sfig{ff06}{Fire burnt out}
\caption{Example of the Forest Fire model. A new vertex (4) is inserted. The boxes
on the edges indicate the edge targets.}
\label{fig:ff-example}
\end{figure}

\begin{samepage}
The authors list 4 ideas that were incorporated into this model:
\begin{itemize}
\item \emph{Rich get richer}, similar to the Preferential Attachment model, to generate a vertex 
degree power law. Here, highly connected vertices are easy to reach through other 
vertices.
\item Edges are \emph{copied} from related vertices, similar to the Copying Model, to 
generate a community structure.
\item The process of \emph{Community Guided Attachment} that leads to the 
densification power law. This part is not very clearly pointed out -- the 
entrance vertices could be set in correlation to the parent vertices in the 
Community Guided Attachment model.
\item An \emph{additional ``ingredient''}, that leads to shrinking diameters. The 
authors assume that the entrance vertices might act as bridges between formerly
unconnected parts of the graph.
\end{itemize}
\end{samepage}

For the Forest Fire Model, the generation of power law vertex degree distributions,
densification power laws and shrinking diameters is not analytically proven. The authors 
only give an empirical proof of these properties. \reffig{ffmid} shows example 
results of the Forest Fire model. The generated graphs follow a densification 
power law and the diameter shrinks. In Figures \ref{fig:ffmidindeg} and 
\ref{fig:ffmidoutdeg}, one can see that the in- and out-degrees of the graph 
seem to follow a power-law distribution with a negative exponent, similar to the 
Preferential Attachment model or the Copying Model.

In a more recent work, \citeauthor{Leskovec2008} state that the Forest Fire 
model further is able to reproduce community structures that are similar to the 
ones observable in real world graphs \cite{Leskovec2008}.

\begin{figure}[ht]
\centering
\begin{subfigure}{.48\textwidth}
\centering
\coordimage{ffmiddeg}{Number of vertices}{Number of edges}
\caption{Densification power law}
\label{fig:ffmiddeg}
\end{subfigure}
~
\begin{subfigure}{.48\textwidth}
\centering
\coordimage{ffmiddiam}{Number of vertices}{Effective Diameter}
\caption{Shrinking diameter}
\label{fig:ffmiddiam}
\end{subfigure}
\\
\begin{subfigure}{.48\textwidth}
\centering
\coordimage{ffmidindeg}{Vertex in-egree}{Number of vertices}
\caption{In-degree distribution}
\label{fig:ffmidindeg}
\end{subfigure}
~
\begin{subfigure}{.48\textwidth}
\centering
\coordimage{ffmidoutdeg}{Vertex out-degree}{Number of vertices}
\caption{Out-degree distribution}
\label{fig:ffmidoutdeg}
\end{subfigure}
\caption{The Forest Fire model with $p = 0.37$ and $r = 0.32$ generates a densifying
graph whose diameter shrinks and that seems to exhibit power-law in- and out-degrees.}
\label{fig:ffmid}
\end{figure}
